# Build
build: build-client build-server

# Build client for ARM architecture
build-client-arm:
	GOOS=linux GOARCH=arm go build -o bin/arm/client ./cmd/client/main.go

# Build client for AMD architecture
build-client-amd64:
	GOOS=linux GOARCH=amd64 go build -o bin/amd64/client ./cmd/client/main.go

# Build client
build-client: build-client-arm build-client-amd64

# Build server for ARM architecture
build-server-arm:
	GOOS=linux GOARCH=arm go build -o bin/arm/server ./cmd/server/main.go

# Build server for AMD architecture
build-server-amd64:
	GOOS=linux GOARCH=amd64 go build -o bin/amd64/server ./cmd/server/main.go

# Build server
build-server: build-server-arm build-server-amd64

run-server-amd64: build-server-amd64
	./bin/amd64/server

run-client-amd64: build-client-amd64
	./bin/amd64/client

run-server-arm: build-server-arm
	./bin/arm/server

run-client-arm: build-client-arm
	./bin/arm/client