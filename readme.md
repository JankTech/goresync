# Go Remarkable Sync

This repository aims to sync data from a remarkable tablet to a remote server.

## Getting started

1. Download one of the ARM releases and copy it over to the remarkable tablet.
You can do this using SCP over SSH when the remarkable tablet is connected to a computer via USB.

2. Set up systemd service and timer on the remarkable. For example:
    
    ```
    # /lib/systemd/system/goresync.service
    [Unit]
    Description=Sync script to back up data

    [Service]
    Type=oneshot
    ExecStart=/home/root/goresync
    WorkingDirectory=/home/root
    ```

    ```
    # /lib/systemd/system/goresync.timer
    [Unit]
    Description=Daily sync script to backup data

    [Timer]
    OnCalendar=daily
    Persistent=true

    [Install]
    WantedBy=timers.target
    ```
3. Restart the systemctl daemon and enable the timer
    ```
    systemctl daemon-reload
    systemctl enable goresync.timer
    ```

## Progress

* An initial attempt to transfer the data over ssh is underway
* A backup plan is to have a webserver to receive the data and handle the uploaded files.