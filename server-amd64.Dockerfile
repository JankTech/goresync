FROM golang:1.20 as build

WORKDIR /go/src/app
COPY . .

RUN mkdir /storage

# RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /go/bin/goresync-server ./cmd/server/main.go

FROM gcr.io/distroless/static-debian11
COPY --from=build /go/bin/goresync-server /
COPY --from=build /storage /

EXPOSE 8080

CMD ["/goresync-server"]
