package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/upload", uploadHandler)

	log.Println("Server started on localhost:8080, use /upload for uploading files")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	// Check the request method is POST which is used to submit forms.
	if r.Method != "POST" {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}

	// Parse multipart/form-data
	err := r.ParseMultipartForm(10 << 20) // Maximum memory 10MB
	if err != nil {
		http.Error(w, "Error parsing the multipart form", http.StatusInternalServerError)
		return
	}

	// Retrieve file from 'zipfile' form data
	file, handler, err := r.FormFile("zipfile")
	if err != nil {
		http.Error(w, "Error retrieving the file", http.StatusInternalServerError)
		return
	}
	defer file.Close()

	fmt.Printf("Uploaded file: %+v\n", handler.Filename)
	fmt.Printf("File size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	// Create destination file
	dst, err := os.OpenFile("./storage/"+handler.Filename, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(fmt.Errorf("error creating the file: %v", err))
		http.Error(w, "Error creating the file", http.StatusInternalServerError)
		return
	}
	defer dst.Close()

	// Copy the uploaded file to the created file on the filesystem
	_, err = io.Copy(dst, file)
	if err != nil {
		log.Println(fmt.Errorf("error writing the file: %v", err))
		http.Error(w, "Error writing the file", http.StatusInternalServerError)
	}
}
