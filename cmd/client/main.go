package main

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func main() {
	log.Println("GoReSync backup started...")

	zipFileName := generateZipFileName()

	// Zip the data
	err := zipData(zipFileName)
	if err != nil {
		log.Fatalf("Error zipping data: %v", err)
	}
	log.Println("Data Zipped. Filename: " + zipFileName)

	// Open the zip file
	file, err := os.Open(zipFileName)
	if err != nil {
		log.Fatalf("Error opening the file: %v", err)
	}
	defer file.Close()

	// Send the file
	err = sendData(file)
	if err != nil {
		log.Fatalf("Error sending the data: %v", err)
	}

	log.Println("Data sent to the server successfully")
}

func sendData(file *os.File) error {
	// Prepare a form that you will submit to the URL
	var requestBody bytes.Buffer
	multiPartWriter := multipart.NewWriter(&requestBody)

	// Add the file field
	fileWriter, err := multiPartWriter.CreateFormFile("zipfile", file.Name())
	if err != nil {
		return fmt.Errorf("error adding the file: %v", err)
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		return fmt.Errorf("error copying the file: %v", err)
	}

	// Important: Close the multipart writer
	// If not closed, your request will be missing the terminating boundary.
	err = multiPartWriter.Close()
	if err != nil {
		return fmt.Errorf("error closing the multipart writer: %v", err)
	}

	// Create a client
	client := &http.Client{}

	// Create a POST request
	req, err := http.NewRequest(http.MethodPost, "http://192.168.1.10:8080/upload", &requestBody)
	if err != nil {
		return fmt.Errorf("error creating the request: %v", err)
	}

	// Set the content type, this will contain the boundary.
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	// Do the request
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error making the request: %v", err)
	}
	defer resp.Body.Close()

	log.Println("Response status:", resp.Status)
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("server returned unexpected status code: %d", resp.StatusCode)
	}

	return nil
}

func generateZipFileName() string {
	tStr := strings.ReplaceAll(time.Now().Format(time.RFC3339), ":", "-")
	return "goresync_backup_" + tStr + ".zip"
}

func zipData(filename string) error {

	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	w := zip.NewWriter(file)
	defer w.Close()

	err = filepath.Walk(".local/share/remarkable/xochitl", generateWalker(w))
	if err != nil {
		return err
	}

	return nil
}

func generateWalker(w *zip.Writer) func(path string, info os.FileInfo, err error) error {
	return func(path string, info os.FileInfo, err error) error {
		fmt.Printf("Crawling: %#v\n", path)
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		file, err := os.Open(path)
		if err != nil {
			return err
		}

		defer file.Close()

		// Ensure that `path` is not absolute; it should not start with "/".
		f, err := w.Create(path)
		if err != nil {
			return err
		}

		_, err = io.Copy(f, file)
		if err != nil {
			return err
		}

		return nil
	}
}
